﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Ghost : MonoBehaviour
{
    private NavMeshAgent navAgent;

    private void Awake()
    {
        navAgent = GetComponent<NavMeshAgent>();
    }

    // Start is called before the first frame update
    IEnumerator Start()
    {
        GameObject goalObj = null;
        goalObj = GameObject.FindObjectOfType<Goal>().gameObject;
        while (null == goalObj)
        {
            yield return new WaitForEndOfFrame();
        }

        navAgent.destination = goalObj.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
